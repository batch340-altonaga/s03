-- [SECTION] Inserting Records - CREATE

-- artists
INSERT INTO artists (name) VALUES ("Rivermaya");
INSERT INTO artists (name) VALUES ("Psy");

-- albums
INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Psy 6", "2012-1-1", 2);
INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Trip", "1996-1-1", 1);

-- songs
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Gangnam Style", 253, "K-Pop", 4);

INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Kisapmata", 279, "OPM", 2);

-- [SECTION] Selecting Records - READ

-- Selecting the song_name and genre of all songs
SELECT song_name, genre FROM songs;

-- Selecting all of the songs
SELECT * FROM songs;

-- Selecting all song_name of all OPM songs
SELECT song_name FROM songs WHERE genre = "OPM";

-- We can use AND and OR for multiple expressions in the WHERE clause
SELECT song_name, length FROM songs WHERE length > 240 AND genre = "K-Pop";
SELECT song_name, length FROM songs WHERE length > 240 OR genre = "OPM";

-- [SECTION] Updating Records - UPDATE

-- Update the length of Gangnam Style to 240
UPDATE songs SET length = 240 WHERE song_name = "Gangnam Style";
-- Note: Removing the WHERE clause will update all rows


-- [SECTION] Deleting Records - DELETE
DELETE FROM songs WHERE genre = "OPM" AND length > 240;
-- Note: Removing the WHERE clause will delete all rows
